import { BehaviorSubject } from "rxjs";
import { Color } from "src/types/Color";
const { floor, ceil, random } = Math;

const getRandomNumber = (min: number, max: number) =>
  floor(random() * (floor(max) - ceil(min)) + ceil(min));

const getRandomRGBValue = () =>
  <Color>{
    r: getRandomNumber(0, 256),
    g: getRandomNumber(0, 256),
    b: getRandomNumber(0, 256),
  };

export class ColorService {
  private _color$ = new BehaviorSubject<Color>({ ...getRandomRGBValue() });

  public color$ = this._color$.asObservable();

  public setColor$(value?: Color): void {
    this._color$.next(value ?? getRandomRGBValue());
  }
}
