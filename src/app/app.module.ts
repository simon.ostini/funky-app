import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { DiscoComponent } from "./components/disco/disco.component";
import { HomeComponent } from "./pages/home/home.component";
import { SecondaryPageComponent } from "./pages/secondary-page/secondary-page.component";

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "secondary", component: SecondaryPageComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    DiscoComponent,
    HomeComponent,
    SecondaryPageComponent,
  ],
  imports: [BrowserModule, RouterModule.forRoot(appRoutes)],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
