import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { ColorService } from "src/app/services/color/color.service";
import { Color } from "src/types/Color";

@Component({
  selector: "app-disco",
  templateUrl: "./disco.component.html",
  styleUrls: ["./disco.component.scss"],
  providers: [ColorService],
})
export class DiscoComponent implements OnInit {
  public bgColor$: Observable<Color> = this.colorService.color$;

  constructor(private colorService: ColorService) {
    this.colorService.color$.subscribe();
  }

  public onDiscoBallClick(): void {
    this.colorService.setColor$();
  }

  public ngOnInit(): void {}
}
